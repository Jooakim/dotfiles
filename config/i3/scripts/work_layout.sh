#!/bin/sh
xrandr --output HDMI-1-2 --off --output HDMI-1-1 --off --output eDP-1-1 --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1-1-3 --off --output DP-1-1-2 --off --output DP-1-1-1 --primary --mode 2560x1440 --pos 1920x0 --rotate normal --output DP-1-2 --off --output DP-1-1 --off
bash ~/.fehbg

i3-msg '[workspace="^(1|2|3|4|)"] move workspace to output DP-1-1-1;'
i3-msg '[workspace="^(5)"] move workspace to output eDP-1-1;'
