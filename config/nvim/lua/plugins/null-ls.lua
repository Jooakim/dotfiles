local ok, null_ls = pcall(require, "null-ls")

if not ok then
  return
end

local b = null_ls.builtins

local sources = {

  -- Python
  b.formatting.isort,
  b.formatting.black,
  -- b.formatting.ruff,

  -- Lua
  b.formatting.stylua,

  -- JavaScript etc
  b.formatting.prettier,
  --b.formatting.prettier.disabled_filetypes.with { "markdown" }

  b.formatting.prettier.with {
    disabled_filetypes = { "markdown" }
  },



  -- Shell
  b.formatting.shfmt,
}

null_ls.setup({
  debug = true,
  sources = sources,
})
