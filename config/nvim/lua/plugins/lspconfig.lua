local present, lspconfig = pcall(require, "lspconfig")

if not present then
  return
end

local M = {}
local utils = require("lspconfig/util")
local path = utils.path

require("plugins.lsp_handlers")

-- Borders for LspInfo window
local win = require("lspconfig.ui.windows")
local _default_opts = win.default_opts

win.default_opts = function(options)
  local opts = _default_opts(options)
  opts.border = "single"
  return opts
end

M.on_attach = function(client, _)
  client.server_capabilities.document_formatting = false
  client.server_capabilities.document_range_formatting = false
end

local default_capabilities = require("cmp_nvim_lsp").default_capabilities()

default_capabilities.textDocument.completion.completionItem = {
  documentationFormat = { "markdown", "plaintext" },
  snippetSupport = true,
  preselectSupport = true,
  insertReplaceSupport = true,
  labelDetailsSupport = true,
  deprecatedSupport = true,
  commitCharactersSupport = true,
  tagSupport = { valueSet = { 1 } },
  resolveSupport = {
    properties = {
      "documentation",
      "detail",
      "additionalTextEdits",
    },
  },
}

local function is_dir(filepath)
  local f = io.open(filepath, "r")
  if f then
    local _, _, code = f:read(1)
    f:close()
    return code == 21
  else
    return false
  end
end

local function get_python_path(workspace)
  -- Use activated virtualenv.
  if vim.env.VIRTUAL_ENV then
    print(vim.env.VIRTUAL_ENV)
    return path.join(vim.env.VIRTUAL_ENV, "bin", "python")
  end

  -- Find and use virtualenv in workspace directory.
  local match

  if not is_dir(path.join(workspace, ".venv")) then
    match = vim.fn.glob(path.join(workspace, ".venv"))

    if match ~= "" then
      local venv = vim.fn.trim(vim.fn.system("cat " .. match))
      local env_path = path.join("/home/joakim/.virtualenvs", venv, "bin/python")
      print(env_path)
      return env_path
    end
  end

  -- Find and use virtualenv via poetry in workspace directory.
  match = vim.fn.glob(path.join(workspace, "poetry.lock"))
  if match ~= "" then
    local venv = vim.fn.trim(vim.fn.system("poetry env info -p"))
    return path.join(venv, "bin", "python")
  end
end

M.setup_lsp = function(attach, capabilities)
  local opts = {
    on_attach = attach,
    capabilities = capabilities,
    flags = {
      debounce_text_changes = 150,
    },
  }

  lspconfig.jdtls.setup(opts)
  -- lspconfig.jsonls.setup(opts)
  lspconfig.rust_analyzer.setup(opts)
  lspconfig.lua_ls.setup({
    on_attach = M.on_attach,
    capabilities = capabilities,

    settings = {
      Lua = {
        diagnostics = {
          globals = { "vim" },
        },
        workspace = {
          library = {
            [vim.fn.expand("$VIMRUNTIME/lua")] = true,
            [vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true,
          },
          maxPreload = 100000,
          preloadFileSize = 10000,
        },
      },
    },
  })
  lspconfig.ts_ls.setup(opts)
  -- lspconfig.svelte.setup(opts)
  -- lspconfig.texlab.setup(opts)

  opts.root_dir = vim.loop.cwd
  opts.on_init = function(client)
    client.config.settings.python.pythonPath = get_python_path(client.config.root_dir)
    client.config.settings.python.analysis.diagnosticMode = 'openFilesOnly'
    -- client.config.settings.python.analysis.extraPaths = '/home/joakim/dev/main/python/src/'
    client.config.settings.python.analysis.autoSearchPaths = false
  end
  lspconfig.pyright.setup(opts)
  -- lspconfig.pylsp.setup(opts)
end

M.setup_lsp(M.on_attach, capabilities)

return M
