require("nvim-treesitter.configs").setup({
  ensure_installed = {
    "java",
    "lua",
    "python",
    "javascript",
    "comment",
    "scala",
    "vim",
    "regex",
    "bash",
    "markdown",
    "markdown_inline",
    "xml",
    "http",
    "json",
    "graphql",
  },
  highlight = {
    enable = true,
  },
  disable = function(lang, buf)
    local max_filesize = 100 * 1024 -- 100 KB
    local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
    if ok and stats and stats.size > max_filesize then
      return true
    end
  end,
})
