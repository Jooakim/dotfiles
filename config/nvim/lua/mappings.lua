local nmap = require("keymap").nmap
local vmap = require("keymap").vmap

-- RestNvim
nmap({ "<leader>re", "<Plug>RestNvim" })
nmap({ "<leader>rp", "<Plug>RestNvimPreview" })
nmap({ "<leader>rr", "<cmd>Telescope rest select_env<CR>" })

-- Markview
-- nmap({ "<leader>p", "<cmd>Markview toggle<CR>" })

nmap({"<C-m>", function ()
  require('menu').open('default')
end
})


-- Telescope
nmap({ "<leader>ff", "<cmd>Telescope find_files find_command=rg,--ignore,--files,--sortr,accessed<cr>" })
nmap({ "<leader>fw", "<cmd>Telescope live_grep<cr>" })
nmap({ "<leader>fb", "<cmd>Telescope buffers<cr>" })
nmap({ "<leader>fh", "<cmd>Telescope help_tags<cr>" })
nmap({ "<leader>fp", "<cmd>Telescope project<cr>" })
nmap({ "<leader>fr", "<cmd>Telescope resume<cr>" })
nmap({ "<leader>fs", "<cmd>Telescope grep_string<cr>" })
nmap({ "<leader>fl", "<cmd>Telescope lsp_document_symbols<cr>" })
nmap({ "<leader>fg", "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>" })
nmap({ "<leader>fj", "<cmd>Telescope jumplist<cr>" })
nmap({ "<leader>fi", "<cmd>Telescope import<cr>" })
nmap({ "<leader>fo", "<cmd>Telescope oldfiles<cr>" })

nmap({ "<C-n>", "<cmd>NvimTreeToggle<cr>" })
nmap({ "<C-,>", "<cmd>Lspsaga outline<cr>" })
nmap({ "gd", "<cmd>Lspsaga goto_definition<CR>" })
nmap({ "gp", "<cmd>Lspsaga peek_definition<CR>" })
nmap({ "gr", "<cmd>Lspsaga incoming_calls<CR>" })
nmap({ "gh", "<cmd>Lspsaga finder<CR>" })
nmap({ "K", "<cmd>Lspsaga hover_doc<CR>" })
nmap({ "ge", "<cmd>Lspsaga show_line_diagnostics<CR>" })
nmap({ "<leader>ca", "<cmd>Lspsaga code_action<CR>" })

-- Movement
nmap({ "<C-h>", "<C-w>h" })
nmap({ "<C-l>", "<C-w>l" })
nmap({ "<C-j>", "<C-w>j" })
nmap({ "<C-k>", "<C-w>k" })

-- Misc
nmap({ "<ESC>", "<cmd> noh <cr>" })
vim.cmd([[let g:VM_maps = {}]])
vim.cmd([[let g:VM_maps['Find Under']         = '<C-d>']])
vim.cmd([[let g:VM_maps['Find Subword Under'] = '<C-d>']])
vim.cmd([[let g:VM_maps["Select Cursor Down"] = '<M-C-Down>']])
vim.cmd([[let g:VM_maps["Select Cursor Up"]   = '<M-C-Up>']])

-- Create a Format function
vim.cmd([[command! Format lua vim.lsp.buf.format()]])

-- LSP
nmap({ "gD", vim.lsp.buf.declaration })
-- nmap({ "gd", vim.lsp.buf.definition })
-- nmap({ "K", vim.lsp.buf.hover })
nmap({ "gi", vim.lsp.buf.implementation })
-- nmap({ "ge", vim.diagnostic.open_float })
-- nmap({ "gr", vim.lsp.buf.references })
nmap({ "<C-k>", vim.lsp.buf.signature_help })
nmap({ "<leader>wa", vim.lsp.buf.add_workspace_folder })
nmap({ "<leader>wr", vim.lsp.buf.remove_workspace_folder })
nmap({ "<leader>D", vim.lsp.buf.type_definition })
nmap({ "<leader>rn", vim.lsp.buf.rename.float })
-- nmap({ "<leader>ca", vim.lsp.buf.code_action })
nmap({ "<leader>fm", vim.lsp.buf.format })

nmap({ "<leader>m", ":lua require('dropbar.api').pick()<CR>" })

-- Neotest
-- nmap({ "<leader>tt", ":lua require('neotest').run.run()<CR>}" })
-- nmap({ "<leader>tf", ":lua require('neotest').run.run(vim.fn.expand('%'))<CR>" })
-- nmap({ "<leader>to", ":lua require('neotest').output.open({ enter = true })<CR>" })

-- Keep visual selection after indentation
vmap({ ">", ">gv" })
vmap({ "<", "<gv" })
