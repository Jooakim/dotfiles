-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
--- Autoformat setting
--- Add any additional autocmds here

local set_autoformat = function(pattern, bool_val)
    vim.api.nvim_create_autocmd({ "FileType" }, {
        pattern = pattern,
        callback = function()
            vim.b.autoformat = bool_val
        end,
    })
end

set_autoformat({ "python" }, true)
set_autoformat({ "md" }, false)
set_autoformat({ "markdown" }, false)
set_autoformat({ "lua" }, true)
set_autoformat({ "yaml" }, false)
