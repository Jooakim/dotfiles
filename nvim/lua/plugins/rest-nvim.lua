return {
    {
        "rest-nvim/rest.nvim",
        keys = {
            {
                "<leader>re",
                "<cmd>Rest run<cr>",
                desc = "Run request under cursor",
            },
            {
                "<leader>rs",
                "<cmd>Rest env select<cr>",
                desc = "Select env file",
            },
        },
    },
}
