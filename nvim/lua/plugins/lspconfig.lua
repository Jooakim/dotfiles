return {
    {
        "neovim/nvim-lspconfig",
        ---@class PluginLspOpts
        opts = {
            autoformat = false,
            diagnostics = {
                virtual_text = false,
            },

            ---@type lspconfig.options
            servers = {
                basedpyright = {},
                -- -- pyright will be automatically installed with mason and loaded with lspconfig
                -- pyright = {
                --     on_init = function(client)
                --         client.config.settings.python.analysis.diagnosticMode = "openFilesOnly"
                --     end,
                -- },
            },
        },
        keys = {
            -- add a keymap to browse plugin files
            -- stylua: ignore
            -- {
            --   "ge",
            --   vim.lsp.diagnostic.show_line_diagnostics(),
            --   desc = "Show line diagnostics",
            -- },
        },
    },
}
