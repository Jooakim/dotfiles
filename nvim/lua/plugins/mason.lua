return {
    {
        "williamboman/mason.nvim",
        opts = {
            ensure_installed = {
                "debugpy",
                "lua-language-server",
                "stylua",
                "shfmt",
                "ruff",
                --"pyright",
                "prettier",
                "markdown-toc",
                "markdownlint-cli2",
                "marksman",
            },
        },
    },
}
