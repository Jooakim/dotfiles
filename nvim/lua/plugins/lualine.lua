return {
    {
        "nvim-lualine/lualine.nvim",
        event = "VeryLazy",
        opts = {
            {
                "filename",
                path = 1,
                symbols = {
                    modified = " ", -- Shows when a buffer is modified
                    readonly = " ", -- Shows when a buffer is readonly
                },
            },
        },
    },
}
