return {
    {
        "nvim-telescope/telescope.nvim",
        keys = {
      -- add a keymap to browse plugin files
      -- stylua: ignore
      {
        "<leader>fw",
        "<cmd>Telescope live_grep<cr>",
        desc = "Grep for text",
      },
            { "<leader>fs", "<cmd>Telescope grep_string<cr>", desc = "Grep for text under cursor" },
            {
                "<leader>fp",
                function()
                    require("telescope").extensions.project.project({})
                end,
                desc = "Find Plugin File",
            },
            { "<leader>fF", LazyVim.pick("files"), desc = "Find Files (Root Dir)" },
            { "<leader>ff", LazyVim.pick("files", { root = false }), desc = "Find Files (cwd)" },
            { "<leader>fr", LazyVim.pick("resume"), desc = "Resume last search" },
        },
        -- change some options
        opts = {
            defaults = {
                layout_strategy = "horizontal",
                layout_config = { prompt_position = "top" },
                sorting_strategy = "ascending",
                winblend = 0,
            },
        },
    },
}
