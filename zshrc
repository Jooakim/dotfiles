# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
# source "$HOME/.vim/plugged/gruvbox/gruvbox_256palette.sh"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="theunraveler"


# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  vi-mode
  fzf
  docker
  docker-compose
  autoswitch_virtualenv
)

source $ZSH/oh-my-zsh.sh

export EDITOR='nvim'


# Make normal mode standard on new line
accept-line() { prev_mode=$KEYMAP; zle .accept-line }
zle-line-init() { zle -K ${prev_mode:-viins} }
zle -N accept-line
zle -N zle-line-init

# Python debugger
alias pudb='python3 -m pudb'
alias pytest='pytest -s --disable-warnings'
alias o=xdg-open

export TF_CPP_MIN_LOG_LEVEL=2
#export PATH=$PATH:/home/joakim/Downloads/geckodriver
export PATH=$PATH:/home/joakim/.local/bin

alias scrot='scrot ~/Pictures/Screenshots/%b%d::%H%M%S.png'
alias notes='cd ~/Documents/notes'
alias mux='tmuxinator'
alias glocal="git log --branches --not --remotes --simplify-by-decoration --decorate --oneline"
alias cleanbranches="git branch --merged master | grep -v master | sed 's/origin\///' | xargs -n 1 git branch --delete"

# Fix the issue of small stty window
alias docker-exec='docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti'
alias docker-ps='docker ps -a --format "table {{.ID}}\t{{.Names}}\t{{.Command}}\t{{.RunningFor}}\t{{.Status}}\t{{.Ports}}"'

export FZF_DEFAULT_COMMAND='rg --files '

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# Disable freezing terminal with Ctrl-S
stty -ixon

source $HOME/programs/z/z.sh
#alias gogogo='z -l | awk "{print \$2}"| fzf'
#alias gogo="cd `gogogo`"


if [ "`hostname`" = dsa206871 ]; then
    source ~/dev/dotfiles/work/sealrc
    source ~/.bin/tmuxinator.zsh
fi

if [ "`hostname`" = joakims-xps ]; then
    source ~/dev/dotfiles/work/rfrc
    source ~/.shoreline.zsh
fi

export BUGJAIL='-javaagent:/opt/BugJail/agent.jar'
# Higher keyboard repeat rate
#xset r rate 230 60
alias vim=nvim

alias copy='xclip -sel clip'


kill_last_paused() {
  # Get the PID of the most recent paused job
  last_job=$(jobs -l | tail -n 1 | awk '{print $3}')
  
  if [[ -z $last_job ]]; then
    echo "No paused jobs found."
  else
    # Kill the process
    kill -9 $last_job
    echo "Killed job with PID $last_job"
  fi
}

# Completions
autoload -U bashcompinit
bashcompinit
eval "$(register-python-argcomplete3 pipx)"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/zsh_completion" ] && \. "$NVM_DIR/zsh_completion"  # This loads nvm bash_completion

export PATH="/usr/local/bin:$PATH"
eval "$(pyenv init -)"

export PYTHONBREAKPOINT=pudb.set_trace



PATH="/home/joakim/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/joakim/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/joakim/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/joakim/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/joakim/perl5"; export PERL_MM_OPT;

# Generated for envman. Do not edit.
[ -s "$HOME/.config/envman/load.sh" ] && source "$HOME/.config/envman/load.sh"

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
export PATH="/opt/cinc-workstation/bin:$PATH"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
