#!/bin/bash
startdir = `pwd`
cd

sudo apt install curl arandr rofi i3 i3lock-fancy rxvt-unicode fonts-font-awesome libdbus-1-dev \
    redshift cmake zsh fonts-powerline python3-pip feh pavucontrol fzf ripgrep fd-find -y

# not sure this is needed
#pip install --user powerline-status

## Install rust and compile i3status-rs
curl https://sh.rustup.rs -sSf | sh

mkdir tmp && cd tmp
git clone https://github.com/greshake/i3status-rust
cd i3status-rust && cargo build --release
sudo cp target/release/i3status-rs /usr/local/bin/i3status-rs



sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

if [ -f .bashrc ]; then
	mv .bashrc .bashrc_old
fi
if [ -f .config ]; then
	mv .config .config_old
fi
if [ -f .zshrc ]; then
	mv .zshrc .zshrc_old
fi

cd $startdir

ln -s `pwd`/bashrc ~/.bashrc
ln -s `pwd`/.zshrc ~/.zshrc
ln -s `pwd`/config ~/.config
ln -s `pwd`/.Xresources ~/.Xresources
ln -s `pwd`/.tmux.conf ~/.tmux.conf
ln -s `pwd`/config/i3 ~/.config/i3
ln -s `pwd`/config/rofi ~/.config/rofi

# Setup keymap switch
mkdir -p /etc/X11/xorg.conf.d
sudo ln -s `pwd`/00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
sudo ln -s `pwd`/50-synaptics.conf /etc/X11/xorg.conf.d/50-synaptics.conf


mkdir -p ~/.local/share/fonts
cp fonts/* ~/.local/share/fonts/

fc-cache -f -v

cd vim
bash auto-build.sh

# Map caps to escape on a system level
# This is now done with the 00-keyboard.conf instead
#sudo sh -c 'echo "XKBOPTIONS=caps:escape" >> /etc/default/keyboard '

