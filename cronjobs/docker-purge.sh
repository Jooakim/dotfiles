#!/bin/bash
docker system prune --force
docker image rm $(docker images -qa)
yes | docker volume prune
