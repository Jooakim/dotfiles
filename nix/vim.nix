with import <nixpkgs> {};
vim_configurable.customize {
  name = "vim";
  vimrcConfig = {
    customRC = ''
        filetype off

        " Jump between splits
        nnoremap <C-j> <C-W><C-J>
        nnoremap <C-k> <C-W><C-K>
        nnoremap <C-l> <C-W><C-L>
        nnoremap <C-h> <C-W><C-H>

        " Enable spell checking
        set spelllang=en
        set spellfile=$HOME/Dropbox/vim/spell/en.utf-8.add
        set spell

        " Enable cold folding
        set foldmethod=indent
        set foldlevel=99

        " Enbale default configs
        set tabstop=4
        set softtabstop=4
        set shiftwidth=4
        set encoding=utf-8
        set textwidth=119
        set expandtab
        set autoindent
        set fileformat=unix
        set relativenumber
        colorscheme gruvbox
        set background=dark
        syntax on
        set clipboard=unnamed
        set cursorline
        " Allow switching from unsaved buffer
        set hidden

        " Python  specific shizzle
        let python_highlight_all=1
        syn match pythonBoolean "\(\W\|^\)\zsself\ze\."

        " Add keybindings for fzf and rg
        nnoremap <C-p> :FZF<CR>
        nnoremap <C-f> :Rg<CR>


        " ALE Shizzle
        " Disable continuous linting
        let g:ale_lint_on_text_changed = 'never'
        " You can disable this option too
        " if you don't want linters to run on opening a file
        let g:ale_lint_on_enter = 0
        let g:ale_fixers = {'python': ['autopep8']}
        let g:ale_linters = {'python': ['flake8']}


        map <C-n> :NERDTreeToggle<CR>

        " Keymapping for replacing word under cursor
        :nnoremap <Leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>

        " Reselct visual selection after indenting
        :vnoremap < <gv
        :vnoremap > >gv


        highlight ExtraWhitespace ctermbg=red guibg=red
        match ExtraWhitespace /\s\+$/
        autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
        autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
        autocmd InsertLeave * match ExtraWhitespace /\s\+$/
        autocmd BufWinLeave * call clearmatches()

        " Disable ex-mode
        nnoremap Q <nop>
        nnoremap <C-y>  :Goyo<CR>
        function! s:goyo_enter()
        silent !i3-msg fullscreen || true
        if executable('tmux') && strlen($TMUX)
        silent !tmux set status off
        "silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
        endif
        "set noshowmode
        "set noshowcmd
        "set scrolloff=999
        endfunction

        function! s:goyo_leave()
        silent !i3-msg fullscreen || true
        if executable('tmux') && strlen($TMUX)
        silent !tmux set status on
        "silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
        endif
        "set showmode
        "set showcmd
        "set scrolloff=5
        " ...
        endfunction

        autocmd! User GoyoEnter nested call <SID>goyo_enter()
        autocmd! User GoyoLeave nested call <SID>goyo_leave()
        let g:goyo_width = 110

        let g:airline#extensions#tabline#enabled                      = 1
        let g:airline#extensions#tabline#buffer_min_count             = 1
        let g:airline#extensions#tabline#tab_min_count                = 1
        let g:airline#extensions#tabline#buffer_idx_mode              = 1
        let g:airline#extensions#tabline#buffer_nr_show               = 0
        let g:airline#extensions#tabline#show_buffers                 = 1
        let g:airline#extensions#branch#enabled                       = 1
        let g:airline#extensions#tabline#left_sep                     = ""
        let g:airline#extensions#tabline#left_alt_sep                 = "|"
        let g:airline#extensions#tabline#right_sep                    = ""
        let g:airline#extensions#tabline#right_alt_sep                = "|"
        let g:airline_left_sep                                        = ""
        let g:airline_left_alt_sep                                    = "|"
        let g:airline_right_sep                                       = ""
        let g:airline_right_alt_sep                                   = "|"
        let g:airline_powerline_fonts                                 = 1
        let g:airline#extensions#tabline#fnamemod                     = ":t"
        let g:airline_theme                                           = "ayu_mirage"

        " Easier tab/buffer switching
        nmap <leader>1 <Plug>AirlineSelectTab1
        nmap <leader>2 <Plug>AirlineSelectTab2
        nmap <leader>3 <Plug>AirlineSelectTab3
        nmap <leader>4 <Plug>AirlineSelectTab4
        nmap <leader>5 <Plug>AirlineSelectTab5
        nmap <leader>6 <Plug>AirlineSelectTab6
        nmap <leader>7 <Plug>AirlineSelectTab7
        nmap <leader>8 <Plug>AirlineSelectTab8
        nmap <leader>9 <Plug>AirlineSelectTab9
        let g:LanguageClient_serverCommands = {
          \ 'java': ['/home/joakim/test.sh', '-data', getcwd()],
          \ 'python': ['pyls'],
          \ 'haskell': ['hie-wrapper']
        \ }

        nnoremap <F5> :call LanguageClient_contextMenu()<CR>
        nnoremap <silent> gh :call LanguageClient_textDocument_hover()<CR>
        nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
        nnoremap <silent> gr :call LanguageClient_textDocument_references()<CR>
        nnoremap <silent> gs :call LanguageClient_textDocument_documentSymbol()<CR>
        nnoremap <silent> <F2> :call LanguageClient_textDocument_rename()<CR>
        nnoremap <silent> gf :call LanguageClient_textDocument_formatting()<CR>

        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        " MULTIPURPOSE TAB KEY
        " Indent if we're at the beginning of a line. Else, do completion.
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        function! InsertTabWrapper()
            let col = col('.') - 1
            if !col || getline('.')[col - 1] !~ '\k'
                return "\<tab>"
            else
                return "\<c-p>"
            endif
        endfunction
        inoremap <expr> <tab> InsertTabWrapper()
        inoremap <s-tab> <c-n>
    '';
    vam.knownPlugins = pkgs.vimPlugins;
    vam.pluginDictionaries = [
      { names = [
        "fzf-vim"
        "gruvbox"
        "goyo"
        "vim-fugitive"
        "vim-sensible"
        "vim-surround"
        "haskell-vim"
        "quickfix-reflector-vim"
        "nerdtree"
        "nerdcommenter"
        "ale"
        "LanguageClient-neovim"
        "vim-airline"
        "vim-airline-themes"
      ];
    }];
  };
}
