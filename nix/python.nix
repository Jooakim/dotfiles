with import <nixpkgs> {};

let
  manyLinuxFile =
    writeTextDir "_manylinux.py"
    ''
    print("in _manylinux.py")
    manylinux1_compatible = True
    '';
    profile = ''
    export PYTHONPATH=${manyLinuxFile.out}:/usr/lib/python3.6/site-packages
    '';
    in {

      buildFHSUserEnv {
        name = "my-python-env";
        targetPkgs = pkgs: with pkgs; [
          python3
          pipenv
          which
          gcc
          binutils

    # All the C libraries that a manylinux_1 wheel might depend on:
    ncurses
    xorg.libX11
    xorg.libXext
    xorg.libXrender
    xorg.libICE
    xorg.libSM
    glib
  ];

  runScript = "$SHELL";
}
}
