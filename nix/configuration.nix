{ config, pkgs, ... }:
let
  all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
in
{

  imports =
    [ # Include the results of the hardware scan.
    /etc/nixos/hardware-configuration.nix
    /etc/nixos/cachix.nix
  ];

  boot.kernelPackages = pkgs.linuxPackages_latest;
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "spectre"; # Define your hostname.
  #networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;

  powerManagement = {
    # run powertop --auto-tune on startup
    powertop.enable = true;
  };


  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Stockholm";

  nixpkgs.config.allowUnfree = true;
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget (import ./vim.nix) i3 firefox git zsh oh-my-zsh rxvt_unicode tmuxinator tmux feh tint2
    docker cmake clang gparted arandr ripgrep neofetch htop tealdeer light blueman
    dropbox-cli ranger pavucontrol spotify jdk12 fzf pipenv networkmanagerapplet killall pciutils
    chromium unzip gcc pkg-config

    hugo

    vlc libmicrodns # Used for chromecast support
    deluge

    ghc
    cabal-install
    # Install stable HIE for GHC 8.6.5
    (all-hies.selection { selector = p: { inherit (p) ghc865; }; })

    (python3.withPackages(ps: [
      ps.requests
      ps.pudb
      ps.matplotlib
      ps.numpy

      # Vim language server packages
      ps.python-language-server
      ps.pyls-isort
      ps.pyls-black
    ]
    ))
  ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.light.enable = true;
  programs.zsh = {
    enable = true;
    promptInit = ""; # Clear this to avoid a conflict with oh-my-zsh
    ohMyZsh = {
      enable = true;
      plugins = [ "git" ];
    };
    interactiveShellInit = ''
      export ZSH=${pkgs.oh-my-zsh}/share/oh-my-zsh/

      # Customize your oh-my-zsh options here
      ZSH_THEME="theunraveler"

      source $ZSH/oh-my-zsh.sh


      export EDITOR='vim'
      set -o vi

      # Disable freezing terminal with Ctrl-S
      stty -ixon
    '';
  };


  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  fonts.fonts = [
    pkgs.font-awesome_4
    pkgs.powerline-fonts
  ];


  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
  };

  programs.ssh.askPassword = "";

  # Enable the X11 windowing system.
  location.provider = "geoclue2";
  services = {
    xserver.enable = true;
    xserver.autorun = true;
    xserver.layout = "us,se";

    redshift = {
      enable = true;
    };

    # automatically change xrandr profiles on display change
    autorandr.enable = true;

    # auto-hibernate on low battery
    upower.enable = true;

    # monitor and manage CPU temp, throttling as needed
    thermald.enable = true;
    xserver = {
      desktopManager.default = "none";
      desktopManager.xterm.enable = false;
      windowManager.i3 = {
        enable = true;
        extraPackages = with pkgs; [
          rofi
          i3status-rust
          i3blocks
          lm_sensors
          acpi
          polybar
        ];
        extraSessionCommands = ''
          xset s off
          xset -dpms
          xset s noblank
        '';
      };

      xkbOptions = "grp:alt_shift_toggle,caps:escape";

      # Enable touchpad support.
      libinput.enable = true;
    };

    # Add workaround for light requireing sudo
    udev.extraRules = ''
       ACTION=="add", SUBSYSTEM=="backlight", RUN+="${pkgs.coreutils}/bin/chgrp video %S%p/brightness", RUN+="${pkgs.coreutils}/bin/chmod g+w %S%p/brightness"
    '';

    udev.path = [
      pkgs.coreutils # for chgrp
    ];
};

networking.firewall = {
  allowedTCPPorts = [ 17500 8010];
  allowedUDPPorts = [ 17500 ];
};

systemd.user.services.dropbox = {
  description = "Dropbox";
  wantedBy = [ "graphical-session.target" ];
  environment = {
    QT_PLUGIN_PATH = "/run/current-system/sw/" + pkgs.qt5.qtbase.qtPluginPrefix;
    QML2_IMPORT_PATH = "/run/current-system/sw/" + pkgs.qt5.qtbase.qtQmlPrefix;
  };
  serviceConfig = {
    ExecStart = "${pkgs.dropbox.out}/bin/dropbox";
    ExecReload = "${pkgs.coreutils.out}/bin/kill -HUP $MAINPID";
    KillMode = "control-group"; # upstream recommends process
    Restart = "on-failure";
    PrivateTmp = true;
    ProtectSystem = "full";
    Nice = 10;
  };
};

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.joakim = {
    isNormalUser = true;
    shell = pkgs.zsh;
    group = "users";
    extraGroups = [ "wheel" "video" "networkmanager"]; # Enable ‘sudo’ for the user.
  };

  system.autoUpgrade.enable = true;
  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?
}
