#!/bin/zsh

function localbroker() {
    sudo cp /etc/hosts.local /etc/hosts
    cp ~/.ssh/secrets.local ~/.ssh/secrets.properties
}

function prodbroker() {
    sudo cp /etc/hosts.prod /etc/hosts
    cp ~/.ssh/secrets.prod ~/.ssh/secrets.properties
}

function build_lab() {
    cd $HOME/dev/workspace/walrus/walrus-models
    bash cd/docker-publish.sh
    cd $HOME/dev/workspace/walrus-lab/walrus-labconductor
    bash cd/docker-publish.sh
    cd $HOME/dev/workspace/walrus-lab/lab-breeze-master
    bash cd/docker-publish.sh
    cd $HOME/dev/workspace/walrus-lab/lab-breeze-worker
    bash cd/docker-publish.sh
}

function env_down() {
    if [ "$1" = "vpcml" ] || [ "$1" = "irondl" ]; then
        tmpfile=$(mktemp /tmp/down.XXXXXX)
        echo "lab.env.type=$1" >> $tmpfile
        echo "lab.env.name=$2" >> $tmpfile
        echo "lab.env.command=down" >> $tmpfile
        echo "lab.experimentName=down" >> $tmpfile
        java -jar $HOME/dev/walrus-lab/walrus-labclient/target/walrus-labclient-1.0.jar -p $tmpfile
        rm $tmpfile
    else
        echo "First argument must be vpcml or irondl"
    fi
}

function fast_build() {
    pushd `pwd`
    module=$(basename `pwd`)
    cd ..
    mvni -pl $module -am
    popd
}

function print_log() {
    service_name_partial=$1
    service_name=$(docker ps | grep $service_name_partial | awk '{print $2}' | sed -e 's/.*\///' | sed -e 's/:.*//g')
    echo "service with name $service_name found"
    container_id=$(docker ps | grep $service_name | cut -c 1-5)
    docker exec $container_id cat /seal/log/$service_name.log
}

alias mvni='mvn clean install -DskipTests -DskipITs -Dskip.unit.tests'
alias mvnc='mvn clean compile -DskipTests -DskipITs -Dskip.unit.tests'

export PATH=/usr/local/cuda-9.1/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-9.1/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/python3.6/dist-packages/jep
export JAVA_OPTS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000

#export FZF_DEFAULT_COMMAND='fd --type f'
export PYTHONPATH=$PYTHONPATH:$HOME/dev/walrus-brain
export SEAL_HOME=$HOME/seal/

alias lab='cd ~/dev/walrus-lab'
alias walrus='cd ~/dev/walrus'
alias brain='cd ~/dev/walrus-brain'
alias cove='cd ~/dev/cove'
alias calypso='cd ~/dev/fermat'
alias notebook='cd ~/dev/walrus-notebook'
alias assets='cd ~/dev/walrus-assets'
alias golocal='cd ~/local-docker/'


function build_breeze_walrus() {
    TRAPZERR() {
      print "Error in command, aborting"
      return $(( 128 + $1 ))
    }
    WALRUS_PATH='/Users/joakim.andersson/dev/walrus'
    cd $WALRUS_PATH
    mvn clean install -pl walrus-models,seal-breeze -am -DskipTests -DskipITs -Dskip.unit.tests
    cd walrus-models
    sed -ir 's/docker push/#docker push/g' cd/docker-publish.sh
    bash cd/docker-publish.sh LOCAL_IMAGE
    mv cd/docker-publish.shr cd/docker-publish.sh
}

function build_breeze() {
    TRAPZERR() {
      print "Error in command, aborting"
      return $(( 128 + $1 ))
    }
    pushd $(pwd)
    build_breeze_walrus
    build_breeze_lab
    popd
}

function build_breeze_lab() {
    TRAPZERR() {
      print "Error in command, aborting"
      return $(( 128 + $1 ))
    }
    LAB_PATH='/Users/joakim.andersson/dev/walrus-lab'
    cd $LAB_PATH
    mvn clean install -pl walrus-labconductor,lab-breeze-worker,lab-breeze-master -am -DskipTests -DskipITs -Dskip.unit.tests
    if [ $? -eq 0 ]; then
        mvn install -Pdocker,skipTests -Ddocker.image.version=LOCAL_IMAGE -f walrus-labconductor/pom.xml
        mvn install -Pdocker,skipTests -Ddocker.image.version=LOCAL_IMAGE -f lab-breeze-master/pom.xml
        mvn install -Pdocker,skipTests -Ddocker.image.version=LOCAL_IMAGE -f lab-breeze-worker/pom.xml
    fi
}
function start_breeze() {
    COMPOSE_FILE="/Users/joakim.andersson/dev/dotfiles/work/docker-compose-breeze.yaml"
    # PREPARE shit
    export CACHE_PRV=$(getkey cache token)
    export DATABASE_PUB=$(getkey database username)
    export DATABASE_PRV=$(getkey database token)
    export BROKER_PUB=$(getkey broker username)
    export BROKER_PRV=$(getkey broker token)

    docker-compose -f $COMPOSE_FILE up -d
}

function stop_breeze() {
    COMPOSE_FILE="/Users/joakim.andersson/dev/dotfiles/work/docker-compose-breeze.yaml"
    docker-compose -f $COMPOSE_FILE down -v
}


function getkey() {
    # getkey cache token
    VAULT_URL="vault.seal-software.net"
    DEVOPS_PATH=":8200/v1/secret/data/dev/ml/"
    DEV_VAULT_TOKEN=$(curl $VAULT_URL/dev)
    DATA=$(curl -s --header "X-Vault-Token: $DEV_VAULT_TOKEN" https://$VAULT_URL$DEVOPS_PATH$1)
    echo $DATA | jq '.data.data.'$2 | tr -d \" 
}

function getvaultdata() {
    # getkey cache token
    VAULT_URL="vault.seal-software.net"
    DEVOPS_PATH=":8200/v1/secret/data/dev/ml/certs"
    DEV_VAULT_TOKEN=$(curl $VAULT_URL/dev)
    DATA=$(curl -s --header "X-Vault-Token: $DEV_VAULT_TOKEN" https://$VAULT_URL$DEVOPS_PATH$1)
    echo $DATA
    #echo $DATA | jq '.data.data.'$2 | tr -d \"
}

function breeze_logs() {
    COMPOSE_FILE="/Users/joakim.andersson/dev/dotfiles/work/docker-compose-breeze.yaml"
    docker-compose -f $COMPOSE_FILE logs -f
}

function send_lab() {
    LAB_PATH='/Users/joakim.andersson/dev/walrus-lab'
    java -jar $LAB_PATH/walrus-labclient/target/*.jar -p $1

}

function install_python_dev_packages() {
    pip install pudb rope flake8 black isort data-science-types neovim line_profiler
}
