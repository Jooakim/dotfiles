#!/bin/zsh

function vpn() {
  sudo /home/joakim/.vpn/rf_openvpn.rb --config /home/joakim/.vpn/RF_jandersson_udp_g3.conf --secrets /home/joakim/.vpn/secrets
}

function vpn2() {
  sudo /home/joakim/.vpn/rf_openvpn.rb --config /home/joakim/.vpn/RF_jandersson_tcp_g3.conf --secrets /home/joakim/.vpn/secrets
}
export JDTLS_HOME=/home/joakim/.local/share/nvim/lsp_servers/jdtls
export RF_ROOT=/home/joakim/dev/main
alias main='cd $RF_ROOT'
alias bageriet='cd /home/joakim/dev/bageriet'
alias rfmachine='node $RF_ROOT/tools/rfmachine/rfmachine.js'
alias shoreline='/home/joakim/programs/shoreline/bin/shoreline'
alias g='$RF_ROOT/gradlew'
alias bagenv='env BAGERIET_CONFIG=/home/joakim/rfconf/bageriet_config.yaml'
alias bcli='/home/joakim/dev/bageriet/.venv/bin/python $RF_ROOT/python/pips/analytics/apps/ml_bakery_api/ml_bakery_api/client/ml_bakery_app.py'
alias brun='/home/joakim/.cache/pypoetry/virtualenvs/ml-lab-zJuG_m0K-py3.8/bin/python /home/joakim/dev/bageriet/bin/run_bageriet_local.sh'
alias mlcatalogue='/home/joakim/dev/bageriet/.venv/bin/python /home/joakim/dev/bageriet/ml_lab/ml_catalogue/api/ml_catalogue_app.py'
alias metabakery='/home/joakim/dev/bageriet/.venv/bin/python /home/joakim/dev/bageriet/ml_lab/meta_bakery/meta_bakery_app.py'
alias gba='RF_TOKEN= RF_PROFILE_EXCLUDE= $RF_ROOT/gradlew build -x test'
alias gbc='RF_TOKEN= RF_PROFILE_EXCLUDE= $RF_ROOT/gradlew clean'
alias shorelineauthetc='shoreline  $RF_ROOT/user/authentication/manifest-proxykit.json $RF_ROOT/attribute/service/manifests/attribute_service.json $RF_ROOT/service/directory/server/manifest.json up -d'
alias editinstall="pip install -e . --config-settings editable_mode=strict"
alias pipdev='pip install -e ."[dev]"'

function nocuda() {
  CUDA_VISIBLE_DEVICES="" "$@"
}

alias bagarn='ssh jandersson@bagarn.recfut.com'

function analyzeRemoteDocumentLocally() {
  doc_id=\"$1\"
  echo "{\"id\": $doc_id}" | rfmachine -p lookup_document | rfmachine -sh extract_text analyze_document resolve_analysis resolve_events/post
}

function analyzeFreeTextLocally() {
  content=\"$1\"
  echo "{\"content\": $content}" | rfmachine -sh dry_document extract_text analyze_document resolve_analysis resolve_events/post

}

function getDockerLogin() {
  AWS_TOKEN=$(aws ecr get-login-password --region us-east-1)
  echo "echo $AWS_TOKEN | docker login --username AWS --password-stdin 328158692677.dkr.ecr.us-east-1.amazonaws.com " | copy
}

function startNotebook() {
  cd ~/dev/ml-lab/
  jupyter notebook &
}

function run_mllab() {
  experiment=$1
  config=$2
  cd /home/joakim/dev/ml-lab
  path_to_experiment=$(find ml_lab/experiments -name "$1*.py")
  path_to_experiment=$(sed 's:/:\.:g'<<<"$path_to_experiment")
  path_to_experiment=$(sed 's:\.py::g'<<<"$path_to_experiment")
  eval python -m "$path_to_experiment" "$2"
}

shtail() {
 v=$1
 shift
 docker exec -it shoreline-shell tail $@ "/opt/log/$v/$v.log"
}

shless() {
 v=$1
 shift
 docker exec -it shoreline-shell less $@ "/opt/log/$v/$v.log"
}

shlnav() {
 v=$1
 shift
 docker exec -it shoreline-shell lnav $@ "/opt/log/$v/$v.log"
}

dryrunlocal() {
  content=$1
  echo "{\"content\": \"$content\"}" | rfmachine -sh dry_document extract_text analyze_document resolve_analysis resolve_events classifier-cyberattack-rus/api/predict
}

predict() {
  # predict -sh <classifier_name> "<sentence>"
  local flag=$1
  local classifier_name=$2
  local sentence=$3

  local curl_response=$(curl -s 'https://sproxy.recfut.com/entity-processor/resolveTaggedFragment/text' \
     -H 'accept: application/json' \
     -H 'Content-Type: application/json' \
     -d '{ "text": "'"${sentence}"'", "id": "123", "adfFormat": true }')

  # Extract the taggedFragment value using jq and store it in a variable
  local tagged_fragment=$(echo "${curl_response}" | jq -r '.taggedFragment' | sed 's/"/\\&/g')

  echo "Tagged Fragment: ${tagged_fragment}"

  local output=$(echo '{"text": "'"${tagged_fragment}"'"}' | rfmachine "${flag}" "classifier-${classifier_name}/api/predict-from-tagged-fragment")

  echo "${output}"
}

tagged_fragment_predict() {
  content=$(printf "%q" $@)
  echo "{\"text\": \"$content\"}" | rfmachine -sh classifier-cyberattack-rus/api/predict-from-tagged-fragment
}

# For fetching hosts by service name
#machines() { curl -s "http://procmon.recfut.com/proc-monitor/query?select=system,properties.rf.proc&where=properties.rf.proc.name=$1*" | jq '.result[] | .system.hostname' | tr \" " " | sort }

machines() {
    curl -s "https://procmon.recfut.com/proc-monitor/query?select=system,properties.rf.proc&where=properties.rf.proc.name=$1*" \
    | jq '.result[] | .system.hostname' \
    | tr -d \" \
    | sort
}

ssh_to_machine() {
    local process_name="$1"
    local selection
    local i=0

    machines_list=($(machines "$process_name"))

    if [ ${#machines_list[@]} -eq 0 ]; then
        echo "No machines found running the process: $process_name"
        return 1
    fi

    echo "Select a machine to SSH into:"
    for machine in "${machines_list[@]}"; do
        i=$((i+1))
        echo "$i. $machine"
    done

    echo -n "Enter your choice (number): "
    read selection

    if [[ $selection -ge 1 && $selection -le ${#machines_list[@]} ]]; then
        local machine=${machines_list[$selection]}
        sshrc "jandersson@$machine"
    else
        echo "Invalid selection!"
    fi
}

function run_in_docker() {
  # Check if a virtual environment is active
  if [ -z "$VIRTUAL_ENV" ]; then
    echo "No virtual environment is active. Please activate one."
    return 1
  fi

  # Needed for some weird shit with AgentClientAuth communication
  chmod 777 /home/joakim/rfconf/rf-agent.unix

  # Run the Docker command with the active virtual environment
  docker run -it --rm \
    -v /home/joakim/rfconf:/home/shoreline/rfconf \
    -v ${VIRTUAL_ENV}:${VIRTUAL_ENV} \
    -v /home/joakim/.pyenv/versions:/home/joakim/.pyenv/versions \
    -v ${RF_ROOT}:${RF_ROOT} \
    -v /opt/huggingface:/opt/huggingface \
    -e RF_ROOT=${RF_ROOT} \
    --network network_shoreline \
    registry.recfut.com/rf/application/main/shoreline/shoreline-shell:java21_20240404_1 \
    ${VIRTUAL_ENV}/bin/$@
    #--workdir ${VIRTUAL_ENV}/bin \
}

